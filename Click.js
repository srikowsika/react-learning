import React, { Component }  from 'react';
import ReactDOM from 'react-dom'

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      count: 0,
    };
  }

  updateCount() {
    this.setState((prevState, props) =>{
      return { count: prevState.count + 1 }
    });
  }

  render() {
    return (<button
              onClick={() => {
        return (this.updateCount());}}>
              Clicked {this.state.count} times </button>);
  }
}

React.render(<App />, document.getElementById('app'));

export default App;