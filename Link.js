import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

function Link() {
    return (
      <Router>
        <div>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/login">Login</Link>
            </li>
            <li>
              <Link to="/register">Register</Link>
            </li>
          </ul>
  
          <hr />
  
          <Route exact path="/" component={Home} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
        </div>
      </Router>
    );
}
function Home() {
    return (
      <div>
        <h2>Home</h2>
      </div>
    );
  }
  
  function Login() {
    return (
      <div>
        <h2>Login</h2>
      </div>
    );
  }
  
  function Register({ match }) {
    return (
      <div>
        <h2>Register</h2>
        <ul>
          <li>
            <Link to={`${match.url}/member`}>Member</Link>
          </li>
          <li>
            <Link to={`${match.url}/non-member`}>Non-member</Link>
          </li>
          
        </ul>
  
       
        <Route
        exact
        path={match.path}
        render={() => <h3>Please select user type.</h3>}
      />
    </div>
  );
}

  
  export default Link;
  