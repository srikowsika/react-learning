<html>
<body>

<h2>ES6 Features</h2>
  
  <h3>Block Scope Operators:</h3>
  <p id="demo"></p>

<script>
var  x = 10;
// Here x is 10
{  
  let x = 2;
  // Here x is 2
}
// Here x is 10
document.getElementById("demo").innerHTML = x;
  </script>  
<h3> Exponential Operator:</h3>
  <div>
  <p>Value:</p> <input type="number" id="name"> 
<p>Expoential Value:</p> <input type="number" id="expo"> 
    <input type="submit"</div>
    <p id="demo"></p>
<script>
var x =
document.getElementById('value');
  var y=document.getElementById('expo');
  console.log(x);
  document.getElementById("demo").innerHTML = x ** y;
</script>
  
 <h3>Default Parameter Values</h3>

<p id="demo"></p>

<script>
  // y is 10 if not passed or undefined
  return x + y;
}
document.getElementById("demo").innerHTML = defaultParameter(5);
  </script>
  <h3>JavaScript Number Methods</h3>

<p>The isSafeInteger() method returns true if the argument is a safe integer.</p>
<p>Otherwise it returns false.</p>

<p id="demo"></p>

<script>
document.getElementById("demo").innerHTML =
Number.isSafeInteger(10) + "<br>" + Number.isSafeInteger(12345678901234567890);
</script>
</script>
</body>
</html>